module.exports = {
  moduleDirectories: [
    'node_modules'
  ],
  transform: {
    "\\.ts?$": ['ts-jest', { /* ts-jest config goes here in Jest */ }],
  },
  transformIgnorePatterns: [
    "[/\\\\]node_modules[/\\\\](?!lodash-es/).+\\.js$"
  ],
}