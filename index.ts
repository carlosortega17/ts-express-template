import cors from 'cors';
import morgan from 'morgan';
import express, { Application } from 'express';
import dotenv from 'dotenv';

dotenv.config();

import run from './src/db';
run()
  .then(() => console.log('Successfully connection'))
  .catch((err) => { throw Error(err); });

import api from './src/routes/api.routes';

const app: Application = express();

app.use(morgan('dev'));
app.use(cors({ origin: '*' }));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/api', api);

app.listen(9009);
