import { Router, Request, Response } from 'express';
import userModel from '../models/user.model';

const route = Router();

route.get('/', (req: Request, res: Response) => {
  return res.status(200).json({
    name: 'status',
    message: 'Ok',
  });
});

route.get('/users',  async (req: Request, res: Response) => {
  const users = await userModel.find().lean().exec();
  return res.status(200).json(users);
});

export default route;
