import { prop, getModelForClass, modelOptions } from '@typegoose/typegoose';

@modelOptions({
  schemaOptions: { timestamps: true, versionKey: false, collection: 'users' },
  options: { customName: 'users',
}})
class UserModel {
  @prop({ required: true, type: String, unique: true })
  public username: string;

  @prop({ required: true, type: String })
  public password: string;
}

const model = getModelForClass(UserModel);

export default model;
