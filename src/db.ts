import mongoose, { connect } from 'mongoose';

mongoose.set('strictQuery', true);

const { DB_URL } = process.env;

async function run() {
  await connect(DB_URL);
}

export default run;
